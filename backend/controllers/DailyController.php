<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\web\UploadedFile;

use app\models\Info;

/**
 * Daily lesson controller
 */
class DailyController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            /*'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],*/
        ];
    }

    public function actionIndex()
    {
        Yii::$app->view->title = 'לימוד יומי';

        $info = Info::findOne(1);

        if(is_null($info)) {
            $info = new Info();
            $info->id = 1;
            $info->title = '';
            $info->body = '';
            $info->image = '';
            $info->save();
        }

        Yii::$app->view->params['data'] = $info;

        return $this->render('index');
    }

    public function actionEditDaily()
    {
        $params = Yii::$app->request->post();

        $errors = [];

        $required_fields = [
            'infoTitle',
            'infoBody',
        ];

        foreach($required_fields as $field) {
            if(!isset($params[$field]) || ((!is_array($params[$field]) && $params[$field] == '') || (is_array($params[$field]) && count($params[$field]) == 0))) 
            {
                $errors[] = $field;
            }
        }

        $file = UploadedFile::getInstanceByName('infoPicture');
        if(is_null($file)) $errors[] = 'infoPicture';

        if(count($errors)>0) {
            $return = [
                'status' => false,
                'errors' => $errors,
            ];
        }
        else 
        {
            $root_dir = Yii::getAlias('@webroot');
            $images_dir = '/upload/images';
            $time_dir = '/'.time();

            if (!file_exists($root_dir.$images_dir)) {
                mkdir($root_dir.$images_dir, 0777, true);
            }

            if (!file_exists($root_dir.$images_dir.$time_dir)) {
                mkdir($root_dir.$images_dir.$time_dir, 0777, true);
            }

            $new_name = $images_dir.$time_dir.'/'.$file->name;

            copy($file->tempName, $root_dir.$new_name);

            $info = Info::find()
                ->where(['id' => (int)$params['infoId']])
                ->one();

            $info->title = $params['infoTitle'];
            $info->body = $params['infoBody'];
            $info->image = $new_name;
            $info->save();

            $return = [
                'status' => true,
            ];
        }

        Yii::$app->response->format = yii\web\Response::FORMAT_JSON;

        return $return;
    }

}
