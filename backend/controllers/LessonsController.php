<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;

use app\models\Lessons;

/**
 * Lessons controller
 */
class LessonsController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            /*'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],*/
        ];
    }

    public function actionIndex()
    {
        Yii::$app->view->title = 'שיעורים';

        $params = Yii::$app->request->get();

        $pageSize = 20;
        $pageIndex = $params['page'] ?? 1;

        $users = Lessons::find()
            ->joinWith('rabbi')
            ->joinWith('lessonType')
            ->joinWith('address')
            ->orderBy(['id' => SORT_DESC])
            ->asArray();

        $provider = new ActiveDataProvider([
            'query' =>  $users,
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);

        $result = $provider->getModels();

        $pagination = new Pagination([
            'totalCount' => $provider->getTotalCount(), 
            'pageSize' => $pageSize,
            'defaultPageSize' => $pageSize
        ]);

        $return = [
            'totalCount' => $provider->getTotalCount(),
            'pageSize' => $pageSize,
            'pageIndex' => $pageIndex,
            'result' => $result,
            'pagination' => $pagination,
        ];

        Yii::$app->view->params['data'] = $return;

        return $this->render('index');
    }

    public function actionEditLessonStatus()
    {
        $params = Yii::$app->request->post();
        
        $lesson = Lessons::find()
            ->where(['id' => $params['lesson_id']])
            ->one();

        if(!is_null($lesson)) {
            $lesson->is_active = $params['lesson_status'];
            $lesson->save();

            $return = [
                'status' => true,
            ];
        }
        else {
            $return = [
                'status' => false,
            ];
        }

        Yii::$app->response->format = yii\web\Response::FORMAT_JSON;

        return $return;
    }
}
