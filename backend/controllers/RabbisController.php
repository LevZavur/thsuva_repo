<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;

use app\models\Rabbis;

/**
 * Rabbis controller
 */
class RabbisController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            /*'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],*/
        ];
    }

    public function actionIndex()
    {
        Yii::$app->view->title = 'רבנים';

        $params = Yii::$app->request->get();

        $pageSize = 20;
        $pageIndex = $params['page'] ?? 1;

        $rabbis = Rabbis::find()
            ->orderBy(['id' => SORT_DESC])
            ->asArray();

        $provider = new ActiveDataProvider([
            'query' =>  $rabbis,
            'pagination' => [
                'pageSize' => $pageSize,
            ],
        ]);

        $result = $provider->getModels();

        $statuses = [
            '0' => 'לא פעיל',
            '1' => 'פעיל',
        ];

        $pagination = new Pagination([
            'totalCount' => $provider->getTotalCount(), 
            'pageSize' => $pageSize,
            'defaultPageSize' => $pageSize
        ]);

        $return = [
            'totalCount' => $provider->getTotalCount(),
            'pageSize' => $pageSize,
            'pageIndex' => $pageIndex,
            'result' => $result,
            'statuses' => $statuses,
            'pagination' => $pagination
        ];

        Yii::$app->view->params['data'] = $return;

        return $this->render('index');
    }

    public function actionGetRabbi()
    {
        $params = Yii::$app->request->post();

        $rabbi = Rabbis::find()
            ->select('id, name, phone, username')
            ->where(['id' => $params['rabbi_id']])
            ->asArray()
            ->one();

        $return = [
            'status' => true,
            'rabbi' => $rabbi,
        ];

        Yii::$app->response->format = yii\web\Response::FORMAT_JSON;

        return $return;
    }

    public function actionEditRabbi()
    {
        $params = Yii::$app->request->post();

        $errors = [];

        $required_fields = [
            'rabbiName',
            'rabbiPhone',
            'rabbiUsername'
        ];

        foreach($required_fields as $field) {
            if(!isset($params[$field]) || ((!is_array($params[$field]) && $params[$field] == '') || (is_array($params[$field]) && count($params[$field]) == 0))) 
            {
                $errors[] = $field;
            }
        }

        if(count($errors)>0) {
            $return = [
                'status' => false,
                'errors' => $errors,
            ];
        }
        else 
        {
            $rabbi = Rabbis::find()
                ->where(['id' => $params['rabbiId']])
                ->one();

            $rabbi->name = $params['rabbiName'];
            $rabbi->phone = $params['rabbiPhone'];
            $rabbi->username = $params['rabbiUsername'];
            if($params['rabbiPassword'] != '') {
                $rabbi->password = md5($params['rabbiPassword']);
            }
            $rabbi->save();

            $return = [
                'status' => true,
                'rabbi' => $rabbi,
            ];
        }

        Yii::$app->response->format = yii\web\Response::FORMAT_JSON;

        return $return;
    }

    public function actionNewRabbi()
    {
        $params = Yii::$app->request->post();

        $errors = [];

        $required_fields = [
            'newRabbiName',
            'newRabbiPhone',
            'newRabbiUsername',
            'newRabbiPassword'
        ];

        foreach($required_fields as $field) {
            if(!isset($params[$field]) || ((!is_array($params[$field]) && $params[$field] == '') || (is_array($params[$field]) && count($params[$field]) == 0))) 
            {
                $errors[] = $field;
            }
        }

        if(count($errors)>0) {
            $return = [
                'status' => false,
                'errors' => $errors,
            ];
        }
        else 
        {
            $rabbi = new Rabbis();
            $rabbi->name = $params['newRabbiName'];
            $rabbi->phone = $params['newRabbiPhone'];
            $rabbi->username = $params['newRabbiUsername'];
            $rabbi->password = md5($params['newRabbiPassword']);
            $rabbi->save();

            $return = [
                'status' => true,
            ];
        }

        Yii::$app->response->format = yii\web\Response::FORMAT_JSON;

        return $return;
    }
}