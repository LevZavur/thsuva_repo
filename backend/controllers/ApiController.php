<?php

namespace backend\controllers;

use Yii;
use yii\rest\Controller;

use app\models\Rabbis;
use app\models\Users;
use app\models\LessonTypes;
use app\models\Addresses;
use app\models\Lessons;
use app\models\Info;

class ApiController extends Controller
{
    public function actionEnter(){
        try{

            $requiredFields = [
                "uuid",
                "device_type"
            ];

            $optionalFields = [
                'onesignal_id'
            ];

            $allFields = array_merge($requiredFields, $optionalFields);
            $resultPostValues = $this->_getPostValues($allFields, $requiredFields);

            if(empty($resultPostValues['status'])){
                return $this->_getBaseErrorResponse([
                    "error_message" => $resultPostValues['errors'][0],
                ]);
            }

            $values = $resultPostValues['data']['values'];

            $user_object = Users::findOne(['uuid' => $values['uuid'], 'device_type' => $values['device_type']]);

            date_default_timezone_set("Asia/Jerusalem");

            if(empty($user_object)) {
                $user_object = new Users();
                $user_object->uuid = $values['uuid'];
                $user_object->device_type = $values['device_type'];
                $user_object->first_enter = date('Y-m-d H:i:s');

                if(isset($values['onesignal_id']) && $values['onesignal_id'] != '') $user_object->onesignal_id = $values['onesignal_id'];

                if(!$user_object->save()) {
                    return $this->_getBaseErrorResponse([
                        "error_message" => 'error in uuid or device_type',
                    ]);
                }
            } else {
                $user_object->last_enter = date('Y-m-d H:i:s');

                if(isset($values['onesignal_id']) && $values['onesignal_id'] != '') $user_object->onesignal_id = $values['onesignal_id'];

                $user_object->save();
            }

            $info_object = Info::findOne(1);
            if(empty($info_object)) {
                return $this->_getBaseErrorResponse([
                    "error_message" => 'error loading app info',
                ]);
            }
            $info_object->image = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$info_object->image;

            $lesson_types = LessonTypes::find()->orderBy(['id'=>SORT_ASC])->all();
            $rabbis = Rabbis::find()->select('id, name, phone')->orderBy(['id'=>SORT_ASC])->all();
            $addresses = Addresses::find()->all();
            $lessons = Lessons::find()->select('id, rabbi_id, lesson_type_id, address_id, info, timestamp, datetime')->where(['=', 'is_active', 1])->andWhere(['>', 'timestamp', mktime(0, 0, 0, date('n'), date('j'), date('Y'))])->all();

            list($current_n, $current_d, $current_year) = explode('/',jdtojewish(unixtojd(),false));

            $data = [
                'lesson_types' => $lesson_types,
                'rabbis' => $rabbis,
                'addresses' => $addresses,
                'lessons' => $lessons,
                'info' => $info_object,
                'current_year' => $current_year
            ];

            return $this->_getBaseSuccessResponse([
                'data' => $data,
            ]);

        } catch (\Exception $exception) {
            return $this->_getBaseErrorResponse([
                'extra_errors' => [$exception->getMessage()]
            ]);
        }
    }

    public function actionLogin(){
        try{

            $requiredFields = [
                "username",
                "password",
                "uuid",
                "device_type"
            ];

            $optionalFields = [];

            $allFields = array_merge($requiredFields, $optionalFields);
            $resultPostValues = $this->_getPostValues($allFields, $requiredFields);

            if(empty($resultPostValues['status'])){
                return $this->_getBaseErrorResponse([
                    "error_message" => $resultPostValues['errors'][0],
                ]);
            }

            $values = $resultPostValues['data']['values'];

            $rabbi_object = Rabbis::findOne(['username' => $values['username'], 'password' => $values['password']]);
            if(empty($rabbi_object)) {
                return $this->_getBaseErrorResponse([
                    "error_message" => "wrong username or password",
                ]);
            } else {
                $user_object = Users::findOne(['uuid' => $values['uuid'], 'device_type' => $values['device_type']]);

                if(empty($user_object)) {
                    return $this->_getBaseErrorResponse([
                        "error_message" => 'error in uuid or device_type',
                    ]);
                } else {
                    $user_object->rabbi_id = $rabbi_object['id'];
                    $user_object->save();
                }

                $rabbi_object->is_activated = 1;
                $rabbi_object->save();
            }

            $data = [
                'id' => $rabbi_object['id'],
                'name' => $rabbi_object['name'],
                'phone' => $rabbi_object['phone']
            ];

            return $this->_getBaseSuccessResponse([
                'data' => $data,
            ]);

        } catch (\Exception $exception) {
            return $this->_getBaseErrorResponse([
                'extra_errors' => [$exception->getMessage()]
            ]);
        }
    }

    public function actionCreateLesson(){
        try{

            $requiredFields = [
                "rabbi_id",
                "lesson_type_id",
                "info",
                "place_id",
                "address",
                "lat",
                "long",
            ];

            $optionalFields = [
                "timestamp",
                "datetime",
            ];

            $allFields = array_merge($requiredFields, $optionalFields);
            $resultPostValues = $this->_getPostValues($allFields, $requiredFields);

            if(empty($resultPostValues['status'])){
                return $this->_getBaseErrorResponse([
                    "error_message" => $resultPostValues['errors'][0],
                ]);
            }

            $values = $resultPostValues['data']['values'];

            $rabbi_object = Rabbis::findOne($values['rabbi_id']);
            if(empty($rabbi_object)) {
                return $this->_getBaseErrorResponse([
                    "error_message" => 'rabbi_id not found',
                ]);
            }

            $lesson_type_object = LessonTypes::findOne($values['lesson_type_id']);
            if(empty($lesson_type_object)) {
                return $this->_getBaseErrorResponse([
                    "error_message" => 'lesson_type_id not found',
                ]);
            }

            $address_object = Addresses::findOne(['place_id' => $values['place_id']]);
            if(empty($address_object)) {
                $address_object = new Addresses();
                $address_object->address = $values['address'];
                $address_object->place_id = $values['place_id'];
                $address_object->lat = $values['lat'];
                $address_object->long = $values['long'];
                if(!$address_object->save()) {
                    return $this->_getBaseErrorResponse([
                        "error_message" => 'error in place_id, address, lat, long',
                    ]);
                }
            }

            date_default_timezone_set('UTC');
            if(empty($values['timestamp'])&&empty($values['datetime'])) {
                return $this->_getBaseErrorResponse([
                    "error_message" => 'send timestamp or datetime',
                ]);
            }

            if(isset($values['datetime'])) {
                list($date, $time) = explode(' ', $values["datetime"]);
                list($jedy, $jedm, $jedd) = explode('-', $date);
                $jd = jewishtojd($jedm, $jedd, $jedy);
                $gd = jdtogregorian($jd);
                list($gdm, $gdd, $gdy) = explode('/', $gd);
                list($timeh, $timem, $times) = explode(':', $time);
                date_default_timezone_set('Asia/Jerusalem');
                $values['timestamp'] = mktime($timeh, $timem, 0, $gdm, $gdd, $gdy);
            }
            else
            {
                date_default_timezone_set('Asia/Jerusalem');
            }

            list($gdd, $gdm, $gdy) = explode('-', date('j-n-Y', $values['timestamp']));
            $jed = jdtojewish(gregoriantojd($gdm, $gdd, $gdy) , false);
            date_default_timezone_set('Asia/Jerusalem');
            
            $jeda = explode('/',$jed);
            $m = array(3, 6, 8, 11, 14, 17, 19);
            $leap = in_array(($jeda[2] % 19), $m) ? 'L' : 'N';
            $time = Yii::$app->formatter->asDate($values['timestamp'],'php:H:i:s');
            $date = preg_replace("|(\d+)/(\d+)/(\d+)|","$3-$1-$2", preg_replace("|/(\d)/|","/0$1/", preg_replace("|^(\d)/|","0$1/", $jed)));

            if($leap == 'N') {
                $date = str_replace('-06-', '-07-', $date);
                if(isset($values['datetime'])) $values['datetime'] = str_replace('-06-', '-07-', $values['datetime']);
            }

            if(isset($values['datetime'])) {
                if($values['datetime'] != "$date $time") {
                    return $this->_getBaseErrorResponse([
                        "error_message" => 'wrong datetime '."$date $time$leap",
                    ]);
                }
            }

            $values["datetime"] = "$date $time$leap";

            $lesson_object = new Lessons();
            $lesson_object->rabbi_id = $rabbi_object->id;
            $lesson_object->lesson_type_id = $lesson_type_object->id;
            $lesson_object->address_id = $address_object->id;
            $lesson_object->info = $values['info'];
            $lesson_object->timestamp = (string)$values['timestamp'];
            $lesson_object->datetime = $values["datetime"];
            $lesson_object->is_active = 1;

            if(!$lesson_object->save()) {
                return $this->_getBaseErrorResponse([
                    "error_message" => 'error in info or timestamp',
                ]);
            }

            $data = [
                'lesson' => $lesson_object,
                'address' => $address_object,
            ];

            return $this->_getBaseSuccessResponse([
                'data' => $data,
            ]);

        } catch (\Exception $exception) {
            return $this->_getBaseErrorResponse([
                'extra_errors' => [$exception->getMessage()]
            ]);
        }
    }

    public function actionEditLesson(){
        try{

            $requiredFields = [
                "id",
                "rabbi_id",
                "lesson_type_id",
                "info",
                "place_id",
                "address",
                "lat",
                "long",
            ];

            $optionalFields = [
                "timestamp",
                "datetime",
            ];

            $allFields = array_merge($requiredFields, $optionalFields);
            $resultPostValues = $this->_getPostValues($allFields, $requiredFields);

            if(empty($resultPostValues['status'])){
                return $this->_getBaseErrorResponse([
                    "error_message" => $resultPostValues['errors'][0],
                ]);
            }

            $values = $resultPostValues['data']['values'];

            $lesson_object = Lessons::findOne($values['id']);
            if(empty($lesson_object)) {
                return $this->_getBaseErrorResponse([
                    "error_message" => 'id not found',
                ]);
            }

            $rabbi_object = Rabbis::findOne($values['rabbi_id']);
            if(empty($rabbi_object)) {
                return $this->_getBaseErrorResponse([
                    "error_message" => 'rabbi_id not found',
                ]);
            }

            $lesson_type_object = LessonTypes::findOne($values['lesson_type_id']);
            if(empty($lesson_type_object)) {
                return $this->_getBaseErrorResponse([
                    "error_message" => 'lesson_type_id not found',
                ]);
            }

            $address_object = Addresses::findOne(['place_id' => $values['place_id']]);
            if(empty($address_object)) {
                $address_object = new Addresses();
                $address_object->address = $values['address'];
                $address_object->place_id = $values['place_id'];
                $address_object->lat = $values['lat'];
                $address_object->long = $values['long'];
                if(!$address_object->save()) {
                    return $this->_getBaseErrorResponse([
                        "error_message" => 'error in place_id, address, lat, long',
                    ]);
                }
            }

            date_default_timezone_set('UTC');
            if(empty($values['timestamp'])&&empty($values['datetime'])) {
                return $this->_getBaseErrorResponse([
                    "error_message" => 'send timestamp or datetime',
                ]);
            }

            if(isset($values['datetime'])) {
                list($date, $time) = explode(' ', $values["datetime"]);
                list($jedy, $jedm, $jedd) = explode('-', $date);
                $jd = jewishtojd($jedm, $jedd, $jedy);
                $gd = jdtogregorian($jd);
                list($gdm, $gdd, $gdy) = explode('/', $gd);
                list($timeh, $timem, $times) = explode(':', $time);
                date_default_timezone_set('Asia/Jerusalem');
                $values['timestamp'] = mktime($timeh, $timem, 0, $gdm, $gdd, $gdy);
            }
            else
            {
                date_default_timezone_set('Asia/Jerusalem');
            }

            list($gdd, $gdm, $gdy) = explode('-', date('j-n-Y', $values['timestamp']));
            $jed = jdtojewish(gregoriantojd($gdm, $gdd, $gdy) , false);
            date_default_timezone_set('Asia/Jerusalem');

            $jeda = explode('/',$jed);
            $m = array(3, 6, 8, 11, 14, 17, 19);
            $leap = in_array(($jeda[2] % 19), $m) ? 'L' : 'N';
            $time = Yii::$app->formatter->asDate($values['timestamp'],'php:H:i:s');
            $date = preg_replace("|(\d+)/(\d+)/(\d+)|","$3-$1-$2", preg_replace("|/(\d)/|","/0$1/", preg_replace("|^(\d)/|","0$1/", $jed)));

            if($leap == 'N') {
                $date = str_replace('-06-', '-07-', $date);
                if(isset($values['datetime'])) $values['datetime'] = str_replace('-06-', '-07-', $values['datetime']);
            }

            if(isset($values['datetime'])) {
                if($values['datetime'] != "$date $time") {
                    return $this->_getBaseErrorResponse([
                        "error_message" => 'wrong datetime '."$date $time$leap",
                    ]);
                }
            }
            
            $values["datetime"] = "$date $time$leap";

            $lesson_object->rabbi_id = $rabbi_object->id;
            $lesson_object->lesson_type_id = $lesson_type_object->id;
            $lesson_object->address_id = $address_object->id;
            $lesson_object->info = $values['info'];
            $lesson_object->timestamp = (string)$values['timestamp'];
            $lesson_object->datetime = $values["datetime"];
            if(!$lesson_object->save()) {
                return $this->_getBaseErrorResponse([
                    "error_message" => 'error in info or timestamp',
                ]);
            }

            $data = [
                'lesson' => $lesson_object,
                'address' => $address_object,
            ];

            return $this->_getBaseSuccessResponse([
                'data' => $data,
            ]);

        } catch (\Exception $exception) {
            return $this->_getBaseErrorResponse([
                'extra_errors' => [$exception->getMessage()]
            ]);
        }
    }

    public function actionCheckAddress() {
        try{

            $requiredFields = [
                "place_id"
            ];

            $optionalFields = [];

            $allFields = array_merge($requiredFields, $optionalFields);
            $resultPostValues = $this->_getPostValues($allFields, $requiredFields);

            if(empty($resultPostValues['status'])){
                return $this->_getBaseErrorResponse([
                    "error_message" => $resultPostValues['errors'][0],
                ]);
            }

            $values = $resultPostValues['data']['values'];

            $address_object = Addresses::findOne(['place_id' => $values['place_id']]);
            if(empty($address_object)) {
                $address_object = new Addresses;
                $address_object->id = NULL;
                $address_object->address = NULL;
                $address_object->place_id = $values['place_id'];
                $address_object->lat = NULL;
                $address_object->long = NULL;
            }

            return $this->_getBaseSuccessResponse([
                'data' => $address_object,
            ]);

        } catch (\Exception $exception) {
            return $this->_getBaseErrorResponse([
                'extra_errors' => [$exception->getMessage()]
            ]);
        }
    }

    protected function _getPostValues($fields, $mandatory = null)
    {
        $result = [
            'status' => true,
            'errors' => [],
            'data' => [
                'values' => [],
            ],
        ];

        if ($mandatory === null) {
            $mandatory = $fields;
        } else {
            if ($mandatory === false) {
                $mandatory = [];
            }
        }

        $requestData = \Yii::$app->request->post();
        $requestMethod = \Yii::$app->request->getMethod();

        if(empty($requestData)){
            if($requestMethod == "POST"){
                $rawBody = \Yii::$app->request->getRawBody();
                $requestData = @json_decode($rawBody,true);
            }
        }

        if(empty($requestData)){
            $requestData = [];
        }

        $values = [];
        foreach ($fields as $field) {
            $value = null;
            if(!empty($requestData[$field])){
                $value = $requestData[$field];
            }
            if ($value) {
                $values[$field] = $value;
            } else {
                if (in_array($field, $mandatory)) {
                    $result['status'] = false;
                    $result['errors'][] = "Missing parameter: {$field}";
                    return $result;
                }
            }
        }
        $result['data']['values'] = $values;
        return $result;
    }

    protected function _getBaseSuccessResponse($params = []){
        $result = [
            'error' => 0,
            'error_message' => null,
            'data' => !empty($params['data']) ? $params['data'] : (isset($params['data']) && is_array($params['data']) ? [] : null),
        ];
        if(!empty($params['tmp_data'])){
            $result['tmp'] = $params['tmp_data'];
        }
        if(!empty($params['extraFields'])){
            foreach ($params['extraFields'] as $extraFieldKey => $extraFieldValue){
                $result[$extraFieldKey] = $extraFieldValue;
            }
        }
        return $result;
    }

    protected function _getBaseErrorResponse($params = []){
        $result = [
            'data' => null,
            'error' => 1,
            'error_message' => "ERROR",
        ];
        $error_code = !empty($params['error_code']) && is_numeric($params['error_code']) ? $params['error_code'] : 1;
        if(!empty($error_code)){
            $result['error'] = $error_code;
        }
        if(!empty($params['error_message'])){
            $result['error_message'] = (string) $params['error_message'];
        }
        if(!empty($params['extra_errors'])){
            $result['extra_errors'] = $params['extra_errors'];
        }
        return $result;
    }
}