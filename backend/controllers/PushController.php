<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\web\UploadedFile;

use app\models\Info;

/**
 * Push controller
 */
class PushController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            /*'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],*/
        ];
    }

    public function actionIndex()
    {
        Yii::$app->view->title = 'פושים';

        return $this->render('index');
    }

    public function actionSendPush()
    {
        $params = Yii::$app->request->post();

        $errors = [];

        $required_fields = [
            'pushText',
            'pushType'
        ];

        foreach($required_fields as $field) {
            if(!isset($params[$field]) || ((!is_array($params[$field]) && $params[$field] == '') || (is_array($params[$field]) && count($params[$field]) == 0))) 
            {
                $errors[] = $field;
            }
        }


        if(count($errors)>0) {
            $return = [
                'status' => false,
                'errors' => $errors,
            ];
        }
        else 
        {
            /*
            $root_dir = Yii::getAlias('@webroot');
            $images_dir = '/upload/images';
            $time_dir = '/'.time();

            if (!file_exists($root_dir.$images_dir)) {
                mkdir($root_dir.$images_dir, 0777, true);
            }

            if (!file_exists($root_dir.$images_dir.$time_dir)) {
                mkdir($root_dir.$images_dir.$time_dir, 0777, true);
            }

            $new_name = $images_dir.$time_dir.'/'.$file->name;

            copy($file->tempName, $root_dir.$new_name);

            $info = Info::find()
                ->where(['id' => (int)$params['infoId']])
                ->one();

            $info->title = $params['infoTitle'];
            $info->body = $params['infoBody'];
            $info->image = $new_name;
            $info->save();
            */

            $content = [
                "en" => $params['pushText']
            ];
        
            $fields = array(
                'app_id' => "fbafdae2-1244-4a93-968d-4abea1e59cc7",
                'included_segments' => [$params['pushType']],
                'contents' => $content
            );

            $fields = json_encode($fields);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8', 'Authorization: Basic OWY3OWQwZjktN2Q2Ny00NjdmLTk5MGEtM2U4MmZmOGYzODRk'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);    

            $response = curl_exec($ch);
            curl_close($ch);

            if($rd = json_decode($response, true)) {
                if(isset($rd['errors'])) {
                    $status2 = false;
                    $error = "";
                    foreach($rd['errors'] as $rer) {
                        $error .= "\r\n".$rer;
                    }
                } else {
                    $status2 = true;
                    $error = "";
                }
            } else {
                $status2 = false;
                $error = "\r\nNo answer";
            }

            $return = [
                'status' => true,
                'status2' => $status2,
                'error_text' => $error,
            ];
        }

        Yii::$app->response->format = yii\web\Response::FORMAT_JSON;

        return $return;
    }

}
