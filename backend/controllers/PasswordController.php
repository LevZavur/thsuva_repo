<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\web\UploadedFile;

use app\models\Admins;

/**
 * Password controller
 */
class PasswordController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            /*'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],*/
        ];
    }

    public function actionIndex()
    {
        Yii::$app->view->title = 'שינוי סיסמה';

        return $this->render('index');
    }

    public function actionEditPassword()
    {
        $params = Yii::$app->request->post();

        $errors = [];

        $required_fields = [
            'newPassword',
        ];

        foreach($required_fields as $field) {
            if(!isset($params[$field]) || ((!is_array($params[$field]) && $params[$field] == '') || (is_array($params[$field]) && count($params[$field]) == 0))) 
            {
                $errors[] = $field;
            }
        }

        if(count($errors)>0) {
            $return = [
                'status' => false,
                'errors' => $errors,
            ];
        }
        else 
        {
            $admin = Admins::find()
                ->where(['id' => 1])
                ->one();

            $admin->password = md5($params['newPassword']);
            $admin->save();

            $return = [
                'status' => true,
            ];
        }

        Yii::$app->response->format = yii\web\Response::FORMAT_JSON;

        return $return;
    }

}
