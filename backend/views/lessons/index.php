<table class="table table-bordered">
    <thead>
        <tr>
            <th>
                ID
            </th>
            <th>
                שם רב
            </th>
            <th>
                נושא
            </th>
            <th>
                תאריך
            </th>
            <th>
                שעה
            </th>
            <th>
                תיאור
            </th>
            <th>
                כתובת
            </th>
            <th>
                ביטול
            </th>
        </tr>
    <thead>
    <tbody>
        <?php
            foreach ($this->params['data']['result'] as $key=>$line)
            {
                ?>
                <tr>
                    <td>
                        <?=$line['id']?>
                    </td>
                    <td>
                        <?=$line['rabbi']['name']?>
                    </td>
                    <td>
                        <?=$line['lessonType']['name']?>
                    </td>
                    <td>
                        <?php 
                            date_default_timezone_set('Asia/Jerusalem');
                        ?>
                        <!--<?=Yii::$app->formatter->asTime($line['timestamp'], 'php:d/m/Y')?>-->
                        <?php
                            $d = Yii::$app->formatter->asDate($line['timestamp'],'php:j');
                            $m = Yii::$app->formatter->asDate($line['timestamp'],'php:n');
                            $y = Yii::$app->formatter->asDate($line['timestamp'],'php:Y');
                            $jd = gregoriantojd($m, $d, $y);
                            $jed = iconv('ISO-8859-8', 'UTF-8', jdtojewish($jd, true, CAL_JEWISH_ADD_GERESHAYIM+CAL_JEWISH_ADD_ALAFIM_GERESH));
                            echo $jed;
                        ?>
                    </td>
                    <td>
                        <?=Yii::$app->formatter->asTime($line['timestamp'], 'php:H:i:s')?>
                    </td>
                    <td>
                        <?=$line['info']?>
                    </td>
                    <td>
                        <?=$line['address']['address']?>
                    </td>
                    <td>
                        <input class="form-check-input editLessonStatus" data-lesson-id="<?=$line['id']?>" type="checkbox" <?=$line['is_active']==1?'':'checked'?>/>
                    </td>
                </tr>
                <?php
            }
        ?>
    </tbody>
</table>
<div dir="RTL">
    <?php
        echo yii\widgets\LinkPager::widget([
            'pagination' => $this->params['data']['pagination'],
            'nextPageLabel' => '<i class="fa fa-angle-left"></i>',
            'prevPageLabel' => '<i class="fa fa-angle-right"></i>',
            'lastPageLabel' => '<i class="fa fa-angle-double-left"></i>',
            'firstPageLabel' => '<i class="fa fa-angle-double-right"></i>',
        ]);
    ?>
</div>