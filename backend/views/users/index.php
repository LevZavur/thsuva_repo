<table class="table table-bordered">
    <thead>
        <tr>
            <th>
                ID
            </th>
            <th>
                כניסה ראשונה
            </th>
            <th>
                כניסה אחרונה
            </th>
            <th>
                רב
            </th>
        </tr>
    <thead>
    <tbody>
        <?php
            foreach ($this->params['data']['result'] as $key=>$line)
            {
                ?>
                <tr>
                    <td>
                        <?=$line['id']?>
                    </td>
                    <td>
                        <?=Yii::$app->formatter->asDatetime($line['first_enter'], "php:H:i:s d/m/Y")?>
                    </td>
                    <td>
                        <?=Yii::$app->formatter->asDatetime($line['last_enter'], "php:H:i:s d/m/Y")?>
                    </td>
                    <td>
                        <?=($line['rabbi'] != NULL)?$line['rabbi']['name']:'לא'?>
                    </td>
                </tr>
                <?php
            }
        ?>
    </tbody>
</table>
<div dir="RTL">
    <?php
        echo yii\widgets\LinkPager::widget([
            'pagination' => $this->params['data']['pagination'],
            'nextPageLabel' => '<i class="fa fa-angle-left"></i>',
            'prevPageLabel' => '<i class="fa fa-angle-right"></i>',
            'lastPageLabel' => '<i class="fa fa-angle-double-left"></i>',
            'firstPageLabel' => '<i class="fa fa-angle-double-right"></i>',
        ]);
    ?>
</div>