<form id="passwordForm" method="POST">
    <div class="form-group">
        <label for="newPassword">סיסמה חדשה</label>
        <input type="text" class="form-control" id="newPassword" name="newPassword" value="" placeholder="הזן סיסמה חדשה">
    </div>
    <div class="form-group">
        <button type="button" class="btn btn-primary" id="savePassword">שמור</button>
    </div>
</form>