<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'אורחים', 'icon' => 'user', 'url' => ['/users']],
                    ['label' => 'רבנים', 'icon' => 'user-secret', 'url' => ['/rabbis']],
                    ['label' => 'שיעורים', 'icon' => 'book', 'url' => ['/lessons']],
                    ['label' => 'לימוד יומי', 'icon' => 'file-text', 'url' => ['/daily']],
                    ['label' => 'פושים', 'icon' => 'comment', 'url' => ['/push']],
                    ['label' => 'סיסמה', 'icon' => 'key', 'url' => ['/password']],
                ],
            ]
        ) ?>

    </section>

</aside>
