<form id="pushForm" method="POST">
    <div class="form-group">
        <label for="pushType">סוג משתמש</label>
        <select class="form-control" id="pushType" name="pushType">
            <option value="All" selected>כלל המשתמשים</option>
            <option value="Rabbis">רבנים</option>
            <option value="Guests">אורחים</option>
        </select>
    </div>
    <div class="form-group">
        <label for="pushText">תוכן</label>
        <textarea rows="5" class="form-control" id="pushText" name="pushText"></textarea>
    </div>
    <div class="form-group">
        <button type="button" class="btn btn-primary" id="sendPush">שלח</button>
    </div>
</form>