<div class="row">
    <div class="col-xs-3">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newRabbi">יצירה</button>
    </div>
</div>
<table class="table table-bordered">
    <thead>
        <tr>
            <th>
                ID
            </th>
            <th>
                שם
            </th>
            <th>
                טלפון
            </th>
            <th>
                סטאטוס
            </th>
            <th>
                עריכה
            </th>
        </tr>
    <thead>
    <tbody>
        <?php
            foreach ($this->params['data']['result'] as $key=>$line)
            {
                ?>
                <tr data-rabbi-id="<?=$line['id']?>">
                    <td>
                        <?=$line['id']?>
                    </td>
                    <td class="rabbiName">
                        <?=$line['name']?>
                    </td>
                    <td class="rabbiPhone">
                        <?=$line['phone']?>
                    </td>
                    <td>
                        <?=$this->params['data']['statuses'][$line['is_activated']]?>
                    </td>
                    <td>
                        <a href="#" data-toggle="modal" data-target="#editRabbi" data-rabbi-id="<?=$line['id']?>"><i class="fa fa-pencil"></i></a>
                    </td>
                </tr>
                <?php
            }
        ?>
    </tbody>
</table>
<div dir="RTL">
    <?php
        echo yii\widgets\LinkPager::widget([
            'pagination' => $this->params['data']['pagination'],
            'nextPageLabel' => '<i class="fa fa-angle-left"></i>',
            'prevPageLabel' => '<i class="fa fa-angle-right"></i>',
            'lastPageLabel' => '<i class="fa fa-angle-double-left"></i>',
            'firstPageLabel' => '<i class="fa fa-angle-double-right"></i>',
        ]);
    ?>
</div>
<div class="modal fade" id="editRabbi" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document"><!--<div class="modal-dialog modal-sm" role="document">-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                <h4 class="modal-title">עדכון</h4>
            </div>
            <div class="modal-body">
                <form id="rabbiForm">
                    <input type="hidden" id="rabbiId" name="rabbiId">
                    <div class="form-group">
                        <label for="rabbiName">שם</label>
                        <input type="text" class="form-control" id="rabbiName" name="rabbiName">
                    </div>
                    <div class="form-group">
                        <label for="rabbiPhone">טלפון</label>
                        <input type="text" class="form-control ltr-input" id="rabbiPhone" name="rabbiPhone">
                    </div>
                    <div class="form-group">
                        <label for="rabbiUsername">שם משתמש</label>
                        <input type="text" class="form-control ltr-input" id="rabbiUsername" name="rabbiUsername">
                    </div>
                    <div class="form-group">
                        <label for="rabbiPassword">סיסמה</label>
                        <input type="password" class="form-control ltr-input" id="rabbiPassword" name="rabbiPassword" placeholder="הכנס סיסמה חדשה כדי להחליף אותה">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                <button type="button" id="saveRabbi" class="btn btn-primary">עדכן</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="newRabbi" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document"><!--<div class="modal-dialog modal-sm" role="document">-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                <h4 class="modal-title">יצירה</h4>
            </div>
            <div class="modal-body">
                <form id="newRabbiForm">
                    <div class="form-group">
                        <label for="newRabbiName">שם</label>
                        <input type="text" class="form-control" id="newRabbiName" name="newRabbiName">
                    </div>
                    <div class="form-group">
                        <label for="newRabbiPhone">טלפון</label>
                        <input type="text" class="form-control ltr-input" id="newRabbiPhone" name="newRabbiPhone">
                    </div>
                    <div class="form-group">
                        <label for="newRabbiUsername">שם משתמש</label>
                        <input type="text" class="form-control ltr-input" id="newRabbiUsername" name="newRabbiUsername">
                    </div>
                    <div class="form-group">
                        <label for="newRabbiPassword">סיסמה</label>
                        <input type="password" class="form-control ltr-input" id="newRabbiPassword" name="newRabbiPassword">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                <button type="button" id="saveNewRabbi" class="btn btn-primary">שמור</button>
            </div>
        </div>
    </div>
</div>
