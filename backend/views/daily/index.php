<form id="infoForm" method="POST">
    <input type="hidden" id="infoId" name="infoId" value="<?=$this->params['data']['id']?>">
    <div class="form-group">
        <label for="infoTitle">נושא</label>
        <input type="text" class="form-control" id="infoTitle" name="infoTitle" value="<?=$this->params['data']['title']?>">
    </div>
    <div class="form-group">
        <label for="infoBody">תוכן</label>
        <textarea rows="5" class="form-control" id="infoBody" name="infoBody"><?=$this->params['data']['body']?></textarea>
    </div>
    <div class="form-group">
        <label>תמונה</label><br>
        <label for="infoPicture">
            <div class="btn btn-primary">בחר קובץ</div>
            <input type="file" class="hidden" id="infoPicture" accept=".jpg,.jpeg,.bmp,.png" name="infoPicture">
        </label>
        <img id="infoPicturePreview" class="picurePreview" src="<?=$this->params['data']['image']==''?'#':$this->params['data']['image']?>" alt="אין תמונה"/>
    </div>
    <div class="form-group">
        <br/><br/>
        <button type="button" class="btn btn-primary" id="saveInfo">שמור</button>
    </div>
</form>