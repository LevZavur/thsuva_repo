var block = false;

$(function() {

    $('#savePassword').on('click', function (event) {

        if(block) return;

        showLoad();

        $('#passwordForm').find('input').parent().removeClass('has-error');

        $.ajax({
            url: "/password/edit-password",
            data: new FormData($("#passwordForm")[0]),
            processData: false,
            contentType: false,
            dataType: 'json',
            type: 'POST',
            success: function(data){
                if(data.status)
                {
                    showCheck();
                }
                else 
                {
                    data.errors.forEach(function(field) {
                        $('#passwordForm').find('input#'+field).parent().addClass('has-error');
                    });
                    hideLoad();
                }
            }
        });

    });

})

function showLoad() {
    block = true;
    //$('#savePassword').html('<i class="fa fa-spin fa-spinner"></i>');
}

function hideLoad() {
    block = false;
    $('#savePassword').html('שמור');
}

function showCheck() {
    $('#savePassword').html('<i class="fa fa-check"></i>');
    $('#savePassword').removeClass('btn-primary');
    $('#savePassword').addClass('btn-success');

    setTimeout(() => {
        $('#savePassword').removeClass('btn-success');
        $('#savePassword').addClass('btn-primary');
        $('#newPassword').val('');
        hideLoad();
    }, 2000);
}