var block = false;

$(function() {

    $('#editRabbi').on('show.bs.modal', function (event) {
        var modal = $(this);
        modal.find('.modal-body input').val("");
        var button = $(event.relatedTarget);
        var rabbi_id = button.data('rabbi-id');

        modal.find('.modal-body input').parent().removeClass('has-error');

        $.post("/rabbis/get-rabbi", {rabbi_id}, function(data) {
            modal.find('.modal-body input#rabbiId').val(rabbi_id);
            modal.find('.modal-body input#rabbiName').val(data.rabbi.name);
            modal.find('.modal-body input#rabbiPhone').val(data.rabbi.phone);
            modal.find('.modal-body input#rabbiUsername').val(data.rabbi.username);            
        }, "json");

    });

    $('#newRabbi').on('show.bs.modal', function (event) {

        var modal = $(this);
        modal.find('.modal-body input').val("");
        modal.find('.modal-body input').parent().removeClass('has-error');
        modal.find('.modal-body input').trigger("change");

    });

    $('#saveRabbi').on('click', function (event) {

        if(block) return;

        block = true;

        $('#editRabbi').find('.modal-body input').parent().removeClass('has-error');

        $.ajax({
            url: "/rabbis/edit-rabbi",
            data: new FormData($("#rabbiForm")[0]),
            processData: false,
            contentType: false,
            dataType: 'json',
            type: 'POST',
            success: function(data){
                if(data.status)
                {
                    var line = $('tr[data-rabbi-id="'+ data.rabbi.id+'"]');
                    line.find('td.rabbiName').text(data.rabbi.name);
                    line.find('td.rabbiPhone').text(data.rabbi.phone);

                    $('#editRabbi').modal('hide');
                }
                else 
                {
                    data.errors.forEach(function(field) {
                        $('#editRabbi').find('.modal-body input#'+field).parent().addClass('has-error');
                    });
                }
                block = false;
            }
        });

    });

    $('#saveNewRabbi').on('click', function (event) {

        if(block) return;

        block = true;

        $('#newRabbi').find('.modal-body input').parent().removeClass('has-error');

        $.ajax({
            url: "/rabbis/new-rabbi",
            data: new FormData($("#newRabbiForm")[0]),
            processData: false,
            contentType: false,
            dataType: 'json',
            type: 'POST',
            success: function(data){
                if(data.status)
                {
                    $('#newRabbi').modal('hide');
                    window.location.reload(false); 
                }
                else 
                {
                    data.errors.forEach(function(field) {
                        $('#newRabbi').find('.modal-body input#'+field).parent().addClass('has-error');
                    });
                }
                block = false;
            }
        });

    });

});