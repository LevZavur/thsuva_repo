$(function() {

    $('.editLessonStatus').on('change', function (event) {

        var lesson_id = $(this).data('lesson-id');

        var lesson_status = event.target.checked ? 0 : 1;

        $.post("/lessons/edit-lesson-status", {lesson_id, lesson_status}, function(data) {
            
            if(!data.status) alert('ERROR');

        }, "json");

    });

});