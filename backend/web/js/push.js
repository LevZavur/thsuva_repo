var block = false;

$(function() {

    $('#sendPush').on('click', function (event) {

        if(block) return;

        showLoad()

        $('#pushForm').find('textarea').parent().removeClass('has-error');

        $.ajax({
            url: "/push/send-push",
            data: new FormData($("#pushForm")[0]),
            processData: false,
            contentType: false,
            dataType: 'json',
            type: 'POST',
            success: function(data){
                if(data.status)
                {
                    //hideLoad(data.status2);
                }
                else 
                {
                    data.errors.forEach(function(field) {
                        if(field=='pushText') {
                            $('#pushForm').find('textarea#'+field).parent().addClass('has-error');
                        }
                    });
                }

                hideLoad(data.status, data.status2, data.error_text);
            }
        });

    });

})

function showLoad() {
    block = true;
    $('#sendPush').html('<i class="fa fa-spin fa-spinner"></i>');
    
}

function hideLoad(status, status2, error_text) {
    block = false;
    $('#sendPush').html('שלח');

    if(status) {
        if(status2) {
            alert('נשלח בהצלחה');
            $('#pushForm').find('textarea#pushText').val("");
            $('#pushForm').find('select#pushType').val("All");
        } else {
            alert('שגיאה!' + error_text);
        }
    }
}