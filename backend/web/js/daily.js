function readURL(input, field) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $(field).attr('src', e.target.result);
            $(field).addClass('picurePreviewFull');
            $(field).attr('alt', 'תמונה');
        }
        reader.readAsDataURL(input.files[0]);
    }
    else
    {
        $(field).attr('src', '#');
        $(field).removeClass('picurePreviewFull');
        $(field).attr('alt', 'אין תמונה');
    }
}

var block = false;

$(function() {

    $('#infoPicture').on('click change', function (event) {
        readURL(this, '#infoPicturePreview');
    });

    $('#infoPicture').on('change', function (event) {
        ext = event.target.value.match(/\.([^.]+)$/)[1];

        switch (ext.toLowerCase()) {
            case 'png':
            case 'bmp':
            case 'jpg':
            case 'jpeg':
                // OK
                break;
            default:
                alert('לא תמונה (PNG/BMP/JPG)');
                event.target.value = '';
        }
    });

    $('#saveInfo').on('click', function (event) {

        if(block) return;

        showLoad()

        $('#infoForm').find('input').parent().removeClass('has-error');
        $('#infoForm').find('input#infoPicture').parent().parent().removeClass('has-error');
        $('#infoForm').find('textarea').parent().removeClass('has-error');

        $.ajax({
            url: "/daily/edit-daily",
            data: new FormData($("#infoForm")[0]),
            processData: false,
            contentType: false,
            dataType: 'json',
            type: 'POST',
            success: function(data){
                if(data.status)
                {
                    //
                }
                else 
                {
                    data.errors.forEach(function(field) {
                        if(field=='infoPicture') {
                            $('#infoForm').find('input#'+field).parent().parent().addClass('has-error');
                        } else {
                            $('#infoForm').find('input#'+field).parent().addClass('has-error');
                            $('#infoForm').find('textarea#'+field).parent().addClass('has-error');
                        }
                    });
                }

                hideLoad();
            }
        });

    });

})

function showLoad() {
    block = true;
    $('#saveInfo').html('<i class="fa fa-spin fa-spinner"></i>');
    
}

function hideLoad() {
    block = false;
    $('#saveInfo').html('שמור');
}