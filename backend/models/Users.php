<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $uuid id from iPhone or Android
 * @property int $device_type 1 - iPhone, 2 - Android
 * @property int $rabbi_id
 * @property string $first_enter
 * @property string $last_enter
 *
 * @property Rabbis $rabbi
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uuid', 'device_type'], 'required'],
            [['device_type', 'rabbi_id'], 'integer'],
            [['first_enter', 'last_enter'], 'safe'],
            [['uuid', 'onesignal_id'], 'string', 'max' => 255],
            [['rabbi_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rabbis::className(), 'targetAttribute' => ['rabbi_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uuid' => 'Uuid',
            'device_type' => 'Device Type',
            'rabbi_id' => 'Rabbi ID',
            'first_enter' => 'First Enter',
            'last_enter' => 'Last Enter',
            'onesignal_id' => 'Onesignal ID'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getRabbi()
    {
        return $this->hasOne(Rabbis::className(), ['id' => 'rabbi_id']);
    }
}
