<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lessons".
 *
 * @property int $id
 * @property int $rabbi_id
 * @property int $lesson_type_id
 * @property int $address_id
 * @property string $info
 * @property string $timestamp
 *
 * @property Rabbis $rabbi
 * @property LessonTypes $lessonType
 * @property Addresses $address
 */
class Lessons extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lessons';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rabbi_id', 'lesson_type_id', 'address_id', 'info', 'is_active', 'timestamp', 'datetime'], 'required'],
            [['rabbi_id', 'lesson_type_id', 'address_id', 'is_active'], 'integer'],
            [['info', 'timestamp', 'datetime'], 'string', 'max' => 255],
            [['rabbi_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rabbis::className(), 'targetAttribute' => ['rabbi_id' => 'id']],
            [['lesson_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => LessonTypes::className(), 'targetAttribute' => ['lesson_type_id' => 'id']],
            [['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => Addresses::className(), 'targetAttribute' => ['address_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rabbi_id' => 'Rabbi ID',
            'lesson_type_id' => 'Lesson Type ID',
            'address_id' => 'Address ID',
            'info' => 'Info',
            'timestamp' => 'Timestamp',
            'datetime' => 'Datetime',
            'is_active' => 'Is Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRabbi()
    {
        return $this->hasOne(Rabbis::className(), ['id' => 'rabbi_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLessonType()
    {
        return $this->hasOne(LessonTypes::className(), ['id' => 'lesson_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(Addresses::className(), ['id' => 'address_id']);
    }
}
