<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rabbis".
 *
 * @property int $id
 * @property string $name
 * @property string $username
 * @property string $password md5
 * @property int $is_activated
 *
 * @property Lessons[] $lessons
 * @property Users[] $users
 */
class Rabbis extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rabbis';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'username', 'password'], 'required'],
            [['is_activated'], 'integer'],
            [['name', 'phone', 'username', 'password'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'phone' => 'Phone',
            'username' => 'Username',
            'password' => 'Password',
            'is_activated' => 'Is Activated',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    /*
    public function getLessons()
    {
        return $this->hasMany(Lessons::className(), ['rabbi_id' => 'id']);
    }
    */

    /**
     * @return \yii\db\ActiveQuery
     */
    /*
    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['rabbi_id' => 'id']);
    }
    */

}
