<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "addresses".
 *
 * @property int $id
 * @property string $address
 * @property string $place_id id from Google
 * @property double $lat
 * @property double $long
 *
 * @property Lessons[] $lessons
 */
class Addresses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'addresses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address', 'place_id', 'lat', 'long'], 'required'],
            [['address', 'place_id', 'lat', 'long'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'address' => 'Address',
            'place_id' => 'Place ID',
            'lat' => 'Lat',
            'long' => 'Long',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    /*
    public function getLessons()
    {
        return $this->hasMany(Lessons::className(), ['address_id' => 'id']);
    }
    */
}
