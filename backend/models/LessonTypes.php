<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lesson_types".
 *
 * @property int $id
 * @property string $name
 *
 * @property Lessons[] $lessons
 */
class LessonTypes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lesson_types';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    /*
    public function getLessons()
    {
        return $this->hasMany(Lessons::className(), ['lesson_type_id' => 'id']);
    }
    */
}
