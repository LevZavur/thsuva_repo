<?php

use yii\db\Migration;

/**
 * Class m191110_155442_init
 */
class m191110_155442_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sqlQuery  = "

          CREATE TABLE `addresses` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `address` varchar(255) NOT NULL,
            `place_id` varchar(255) NOT NULL COMMENT 'id from Google',
            `lat` varchar(255) NOT NULL,
            `long` varchar(255) NOT NULL,
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

          CREATE TABLE `admins` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `username` varchar(255) NOT NULL,
            `password` varchar(255) NOT NULL COMMENT 'md5',
            `auth_key` varchar(32) NOT NULL DEFAULT '0',
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

          INSERT INTO `admins` (`id`, `username`, `password`, `auth_key`) VALUES
          (1,	'admin',	'81dc9bdb52d04dc20036dbd8313ed055',	'0');

          CREATE TABLE `info` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `title` varchar(255) NOT NULL,
            `body` text NOT NULL,
            `image` varchar(255) NOT NULL,
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

          INSERT INTO `info` (`id`, `title`, `body`, `image`) VALUES
          (1,	'',	'',	'');

          CREATE TABLE `rabbis` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `name` varchar(255) NOT NULL,
            `phone` varchar(255) NOT NULL,
            `username` varchar(255) NOT NULL,
            `password` varchar(255) NOT NULL COMMENT 'md5',
            `is_activated` tinyint(1) NOT NULL DEFAULT '0',
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

          CREATE TABLE `users` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `uuid` varchar(255) NOT NULL COMMENT 'id from iPhone or Android',
            `device_type` int(11) NOT NULL COMMENT '1 - iPhone, 2 - Android',
            `onesignal_id` varchar(255) DEFAULT NULL,
            `rabbi_id` int(11) DEFAULT NULL,
            `first_enter` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `last_enter` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`),
            KEY `rabbi_id` (`rabbi_id`),
            CONSTRAINT `users_ibfk_1` FOREIGN KEY (`rabbi_id`) REFERENCES `rabbis` (`id`)
          ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

          CREATE TABLE `lesson_types` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `name` varchar(255) NOT NULL,
            PRIMARY KEY (`id`)
          ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

          CREATE TABLE `lessons` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `rabbi_id` int(11) NOT NULL,
            `lesson_type_id` int(11) NOT NULL,
            `address_id` int(11) NOT NULL,
            `info` varchar(255) NOT NULL,
            `timestamp` varchar(255) NOT NULL,
            `datetime` varchar(255) NOT NULL,
            `is_active` tinyint(4) NOT NULL DEFAULT '1',
            PRIMARY KEY (`id`),
            KEY `rabbi_id` (`rabbi_id`),
            KEY `lesson_type_id` (`lesson_type_id`),
            KEY `address_id` (`address_id`),
            CONSTRAINT `lessons_ibfk_1` FOREIGN KEY (`rabbi_id`) REFERENCES `rabbis` (`id`),
            CONSTRAINT `lessons_ibfk_2` FOREIGN KEY (`lesson_type_id`) REFERENCES `lesson_types` (`id`),
            CONSTRAINT `lessons_ibfk_3` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`id`)
          ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

        ";

        $connection = \Yii::$app->getDb();
        $command = $connection->createCommand($sqlQuery);
        $executeResult = $command->execute();

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        /*
        echo "m191110_155442_init cannot be reverted.\n";
        return false;
        */
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191110_155442_init cannot be reverted.\n";

        return false;
    }
    */
}
