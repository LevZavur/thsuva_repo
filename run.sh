#!/bin/sh
apt update
apt install poppler-utils -y
apt clean
docker-php-ext-install calendar
#chown -R www-data.www-data /app
cd /app && composer install
#cd /app/backend && mkdir tmp
cat /etc/apache2/sites-enabled/000-default.conf
sed -i 's|/app/web|/app/backend/web|' /etc/apache2/sites-enabled/000-default.conf
apache2-foreground
